# Migration mercurial to git

Scripts for migrating mercurial repositories to git 

## Getting Started

Give permission for execution 

```
chmod +x getRepos/clonar.sh
```

```
chmod +x borrarRepos/delete.sh
```

```
chmod +x borrarRepos/update.sh
```

### Prerequisites

Requires python 2 and pip to run the script. 

Preferably create a virtual environment 

Edit file list.txt 

Edit the file list.txt with the names of the repositories

### Installing

```
cd getRepos
```


```
./clonar.sh
```

Then 


```
cd .. 
```


```
cd borrarRepos
```

Edit the file list.txt with the names of the repositories


```
./delete.sh
```


```
./edit.sh
```




