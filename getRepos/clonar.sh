#!/bin/bash
echo "----------- Mercurial Migration to Git ------------"
echo "1.- Reading file list.txt"
input="list2.txt"
sleep 5
echo "2.- Creating a folder to save the migrations to git"
mkdir "migrations"
sleep 5
echo "3.- Creating a folder to save mercurial repositories"
mkdir "mercurial"
sleep 5
echo "3.- Cloning migration tool"
git clone git://repo.or.cz/fast-export.git
echo "4.- Installing Mercurial"
pip install mercurial
echo "5.- Starting the migration process"
sleep 5
while IFS= read -r line
do
    echo "#Mercury repository: $line"
    sleep 5
    if [ ! -d "$line" ]; then
            mkdir "migrations/$line-git"
            echo " --> Cloning repository in migrations/$line-git"
            sleep 5
            hg clone "ssh://hg@bitbucket.org/raindrops/$line" "mercurial/$line"
            cd "migrations/$line-git"
            git init
            echo " --> Migrating to Git"
            sleep 5
            ../../fast-export/hg-fast-export.sh  -r ../../"mercurial/$line"
            git checkout HEAD
            echo " --> Creating Bitbucket Repository"
            curl -X POST -v -u hopechan:Matmatiks1024 -H "Content-Type: application/json" "https://api.bitbucket.org/2.0/repositories/raindrops/$line-git" -d '{"scm": "git", "is_private": "true", "fork_policy": "no_public_forks"}'
            echo " --> Pushing to master branch"
            git remote add origin "git@bitbucket.org:raindrops/$line-git.git"
            git push -u origin master            
            cd ../../
        fi
done < "$input"